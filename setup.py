from setuptools import setup

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(name='crawler',
      version='0.1',
      description='LighTricks Crawler',
      url='https://bitbucket.org/111yoav/job_hunt_crawler',
      author='111yoav',
      author_email='111yoav@gmail.com',
      license='MIT',
      packages=['crawler'],
      install_requires=requirements,
      zip_safe=False)
