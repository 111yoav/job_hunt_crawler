import time
import responses
from datetime import datetime, timedelta

from crawler.crawler_api import Crawler
from .utils import generate_url, VALID_HEADERS


def get_sample_headers(last_modified_date=None, etag_value=None):
    """
    Generate custom headers for http response that will be processed by the crawler.
    :param last_modified_date:  Last-Modified header value(optional)
    :type datetime
    :param etag_value: ETag header value(optional)
    :type str

    :return: headers
    :rtype dict
    """
    headers = VALID_HEADERS.copy()
    if last_modified_date:
        headers['Last-Modified'] = last_modified_date.strftime('%a, %d %b %Y %H:%M:%S GMT')
    if etag_value:
        headers['ETag'] = etag_value
    return headers


@responses.activate
def test_cache_update():
    """
    Assert cache updates after crawler action
    """
    empty_cache = dict()
    test_url = generate_url()
    responses.add(responses.GET, test_url, headers=VALID_HEADERS, body='Web Content')

    c = Crawler(cache=empty_cache)
    c.crawl_urls([test_url], depth=1)
    assert test_url in empty_cache


@responses.activate
def test_last_modified_cache():
    """
    Test 3 cases in regard to caching based on the Last-Modified header:
    1. Url with no last-modified value - should update at every crawl
    1. Url with last-modified value that updated between runs - should update
    1. Url with last-modified value that doesn't update between runs - shouldn't update
    """

    test_cache = dict()
    url_with_no_last_modified_header = generate_url('a')
    responses.add(responses.GET, url_with_no_last_modified_header, headers=VALID_HEADERS, body='Cached Content')

    first_last_modified = datetime.now()
    later_last_modified = datetime.now() + timedelta(hours=1)

    url_with_changed_last_modified = generate_url('b')
    first_last_modified_headers = get_sample_headers(last_modified_date=first_last_modified)
    later_last_modified_headers = get_sample_headers(last_modified_date=later_last_modified)

    responses.add(responses.GET, url_with_changed_last_modified, headers=first_last_modified_headers, body='Cached Content')
    responses.add(responses.HEAD, url_with_changed_last_modified, headers=first_last_modified_headers, body=b'')

    url_with_valid_last_modified = generate_url('c')
    responses.add(responses.GET, url_with_valid_last_modified, headers=later_last_modified_headers, body='Cached Content')
    responses.add(responses.HEAD, url_with_valid_last_modified, headers=later_last_modified_headers, body=b'')

    test_urls = [url_with_no_last_modified_header,
                 url_with_changed_last_modified,
                 url_with_valid_last_modified]

    crawler = Crawler(cache=test_cache)
    crawler.crawl_urls(test_urls, depth=1)

    assert all([url in crawler.cache for url in test_urls])
    for url in test_urls:
        responses.remove(responses.GET, url)
        responses.remove(responses.HEAD, url)

    responses.add(responses.GET, url_with_no_last_modified_header, headers=VALID_HEADERS, body='Updated Content')

    responses.add(responses.GET, url_with_changed_last_modified, headers=later_last_modified_headers, body='Updated Content')
    responses.add(responses.HEAD, url_with_changed_last_modified, headers=later_last_modified_headers, body=b'')

    responses.add(responses.GET, url_with_valid_last_modified, headers=later_last_modified_headers, body='Updated Content')
    responses.add(responses.HEAD, url_with_valid_last_modified, headers=later_last_modified_headers, body=b'')
    results = crawler.crawl_urls(test_urls, depth=1)

    assert results[url_with_no_last_modified_header].response.content == b'Updated Content'
    assert results[url_with_changed_last_modified].response.content == b'Updated Content'
    assert results[url_with_valid_last_modified].response.content == b'Cached Content'


@responses.activate
def test_etag_cache():
    """
    Test 3 cases in regard to caching based on the ETag header:
    1. Url with no ETag value - should update at every crawl
    1. Url with ETag value that updated between runs - should update
    1. Url with ETag value that doesn't update between runs - shouldn't update
    """

    test_cache = dict()
    url_with_no_etag_header = generate_url('a')
    responses.add(responses.GET, url_with_no_etag_header, headers=VALID_HEADERS, body='Cached Content')

    url_with_changed_etag = generate_url('b')
    first_etag_headers = get_sample_headers(etag_value='1')
    second_etag_headers = get_sample_headers(etag_value='2')

    responses.add(responses.GET, url_with_changed_etag, headers=first_etag_headers, body='Cached Content')
    responses.add(responses.HEAD, url_with_changed_etag, headers=first_etag_headers, body=b'')

    url_with_consistent_etag = generate_url('c')
    responses.add(responses.GET, url_with_consistent_etag, headers=second_etag_headers, body='Cached Content')
    responses.add(responses.HEAD, url_with_consistent_etag, headers=second_etag_headers, body=b'')

    test_urls = [url_with_no_etag_header,
                 url_with_changed_etag,
                 url_with_consistent_etag]

    crawler = Crawler(cache=test_cache)
    crawler.crawl_urls(test_urls, depth=1)

    assert all([url in crawler.cache for url in test_urls])
    for url in test_urls:
        responses.remove(responses.GET, url)
        responses.remove(responses.HEAD, url)

    responses.add(responses.GET, url_with_no_etag_header, headers=VALID_HEADERS, body='Updated Content')

    responses.add(responses.GET, url_with_changed_etag, headers=second_etag_headers, body='Updated Content')
    responses.add(responses.HEAD, url_with_changed_etag, headers=second_etag_headers, body=b'')

    responses.add(responses.GET, url_with_consistent_etag, headers=second_etag_headers, body='Updated Content')
    responses.add(responses.HEAD, url_with_consistent_etag, headers=second_etag_headers, body=b'')
    results = crawler.crawl_urls(test_urls, depth=1)

    assert results[url_with_no_etag_header].response.content == b'Updated Content'
    assert results[url_with_changed_etag].response.content == b'Updated Content'
    assert results[url_with_consistent_etag].response.content == b'Cached Content'


@responses.activate
def test_cache_recent_entries():
    """
    Test cache access for the same url multiple times against cache time frame.

    Queries withing the defined ttl should use the cache and fetch new results after said
    time period
    """
    test_cache = dict()
    test_url = generate_url()
    responses.add(responses.GET, test_url, headers=VALID_HEADERS, body='Cached Content')
    crawler = Crawler(test_cache, response_cache_time_frame_in_seconds=5)
    results = crawler.crawl_urls([test_url], 1)
    assert results[test_url].response.content == b'Cached Content'

    # Update the response and query immediately (should still use cache)
    responses.remove(responses.GET, test_url)
    responses.add(responses.GET, test_url, headers=VALID_HEADERS, body='Updated Content')
    cached_results = crawler.crawl_urls([test_url], 1)
    assert cached_results[test_url].response.content == b'Cached Content'

    # Wait and query again
    time.sleep(10)
    updated_results = crawler.crawl_urls([test_url], 1)
    assert updated_results[test_url].response.content == b'Updated Content'
