from urllib.parse import urljoin

INVALID_URL = 'aaaabbasdvadvgasags'
VALID_HEADERS = {'Content-Type': 'text/html'}


def generate_url(uri=None):
    return urljoin('http://example.com', str(uri) if uri else None)


def generate_html(url_list):
    if not isinstance(url_list, list):
        url_list = [url_list]

    inner_content = ''.join(['<a href={url}></a>'.format(url=url) for url in url_list])
    return """
    <html>
    <head></head>
    <body>{content}</body>
    </html>
    """.format(content=inner_content)