from crawler.crawler_api import Crawler
from .utils import generate_url, generate_html

import sys
import responses

INVALID_URL = 'aaaabbasdvadvgasags'
VALID_HEADERS = {'Content-Type': 'text/html'}


@responses.activate
def test_sanity():
    test_url = generate_url()
    responses.add(responses.GET, test_url, headers=VALID_HEADERS, body='Test Content')

    c = Crawler(cache=dict())
    results = c.crawl_urls([test_url], depth=1)
    assert results[test_url].response.content == b'Test Content'


@responses.activate
def test_no_head_response():
    test_url = generate_url()
    responses.add(responses.GET, test_url, headers=VALID_HEADERS, body='Test Content')
    c = Crawler(cache=dict())
    results = c.crawl_urls([test_url], depth=1)
    assert results[test_url].response.content == b'Test Content'


@responses.activate
def test_no_mime_header():
    """
    Assert that
    """
    test_url = generate_url()
    responses.add(responses.GET, test_url, headers={}, body='Test Content')
    responses.add(responses.HEAD, test_url, headers={}, body='Test Content')
    c = Crawler(cache=dict())
    results = c.crawl_urls([test_url], depth=1)
    assert test_url not in results


@responses.activate
def test_wrong_mime_header():
    """
    Assert that
    """
    test_url = generate_url()
    responses.add(responses.GET, test_url, headers={'Content-Type': 'text/plain'}, body='Test Content')
    responses.add(responses.HEAD, test_url, headers={'Content-Type': 'text/plain'}, body='Test Content')
    c = Crawler(cache=dict())
    results = c.crawl_urls([test_url], depth=1)
    assert test_url not in results



@responses.activate
def test_recursive_urls():
    """
    Crawl urls that contain an link to each other in their html.
    """

    main_url = generate_url('main')
    feedback_loop_url = generate_url('feedback')
    responses.add(responses.GET, main_url, headers=VALID_HEADERS, body=generate_html(feedback_loop_url))
    responses.add(responses.GET, feedback_loop_url, headers=VALID_HEADERS, body=generate_html(main_url))

    c = Crawler(cache=dict())
    results = c.crawl_urls([main_url], depth=10)
    assert main_url in results
    assert feedback_loop_url in results

    results = c.crawl_urls([feedback_loop_url], depth=10)
    assert main_url in results
    assert feedback_loop_url in results
    assert len(results) == 2


@responses.activate
def test_deep_crawling():
    TEST_DEPTH = sys.getrecursionlimit()*2
    main_url = generate_url('main')
    current_url = main_url
    for i in range(TEST_DEPTH):
        next_url = generate_url(str(i))
        responses.add(responses.GET, current_url, headers=VALID_HEADERS, body=generate_html(next_url))
        responses.add(responses.HEAD, current_url, headers=VALID_HEADERS, body=b'')
        current_url = next_url

    c = Crawler(cache=dict())
    results = c.crawl_urls([main_url], depth=TEST_DEPTH)
    assert len(results) == TEST_DEPTH


@responses.activate
def test_empty_crawling():
    c = Crawler(cache=dict())
    results = c.crawl_urls([], depth=1000)
    assert len(results) == 0


@responses.activate
def test_non_existing_url():
    c = Crawler(cache=dict())
    results = c.crawl_urls([INVALID_URL], depth=1000)
    assert len(results) == 0


@responses.activate
def test_valid_and_invalid_url_mixin():
    """
    Crawl urls with a mix of valid and invalid options in the traversal path and see all valid paths are returned.
    Current Structure is:

    A (valid) -> A1(invalid)

    B (invalid)

    C (valid) -> C1 (valid) -> D (valid)
              -> C2 (invalid)
    """
    test_urls = []
    valid_url_a = generate_url('valid_a')
    invalid_url_a1 = INVALID_URL + '_a1'
    responses.add(responses.GET, valid_url_a, headers=VALID_HEADERS, body=generate_html(invalid_url_a1))

    invalid_url_b = INVALID_URL + '_b'

    valid_url_c = generate_url('valid_c')
    valid_url_c1 = generate_url('valid_c1')
    invalid_url_c2 = INVALID_URL + '_c'
    responses.add(responses.GET, valid_url_c, headers=VALID_HEADERS, body=generate_html([valid_url_c1,
                                                                                         invalid_url_c2]))

    valid_url_d = generate_url('valid_d')
    responses.add(responses.GET, valid_url_c1, headers=VALID_HEADERS, body=generate_html(valid_url_d))
    responses.add(responses.GET, valid_url_d, headers=VALID_HEADERS, body=b'')

    c = Crawler(cache=dict())
    results = c.crawl_urls([valid_url_a,
                            invalid_url_b,
                            valid_url_c], depth=1000)
    assert all(expected_url in results for expected_url in [valid_url_a,
                                                            valid_url_c,
                                                            valid_url_c1,
                                                            valid_url_d])


