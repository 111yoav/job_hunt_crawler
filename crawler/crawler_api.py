from urllib.parse import urljoin, urlsplit
from collections import namedtuple
from bs4 import BeautifulSoup
from datetime import datetime
import os, posixpath, ntpath
from diskcache import Cache
from csv import writer
import pathvalidate
import requests

from crawler.logger import get_logger

Task = namedtuple('Task', ('url', 'depth'))
UrlInfo = namedtuple('UrlInfo', ('request_time', 'response', 'extracted_urls'))
WebCrawlResult = namedtuple('WebCrawlResult', ('url', 'depth', 'response', 'extracted_urls'))


class Crawler(object):
    CACHE_NAME = 'light_tricks_crawler_cache'
    ALLOWED_MIME_TYPES = ['text/html']

    def __init__(self, cache=None, response_cache_time_frame_in_seconds=0):
        """
        Crawler utility.

        :param cache: key-value cache to be used to store requests, by default uses a local DiskCache
        :type Mapping
        :param response_cache_time_frame_in_seconds: time frame in which same url would
        be fetched from cache (if exists) regardless any other parameters. Can be used to allow
         short-time caching of 'unversioned' pages(ones without standard cache headers).

        """
        self.logger = get_logger()

        if cache is None:
            self.cache = Cache(self.CACHE_NAME)
        else:
            self.cache = cache
        self.response_cache_time_frame_in_seconds = response_cache_time_frame_in_seconds
        self.crawler_tasks = list()

    def crawl_urls(self, list_of_urls, depth):
        """
        Recursively crawls a list of urls, fetching the page for each one and crawling each page's inner urls
        up to a given depth.

        :param list_of_urls: list of formatted urls
        :param depth: depth limit
        :return: mapping of url -> WebCrawlResult object
        """
        self.crawler_tasks = [Task(url, depth) for url in list_of_urls]
        results = dict()

        while len(self.crawler_tasks):
            current_task = self.crawler_tasks.pop()
            self.logger.info('iterating {url}'.format(url=current_task.url))
            self.logger.info('{num} tasks left'.format(num=len(self.crawler_tasks)))

            if current_task.url in results:
                self.logger.info('{url} already in results'.format(url=current_task.url))
                continue

            try:
                url_info = self.get_url_info(current_task.url)
                if not url_info:
                    continue
                results[current_task.url] = WebCrawlResult(url=current_task.url,
                                                           depth=current_task.depth,
                                                           response=url_info.response,
                                                           extracted_urls=url_info.extracted_urls)
                if current_task.depth > 1:
                    for internal_url in url_info.extracted_urls:
                        self.crawler_tasks.append(Task(internal_url, current_task.depth-1))
            except Exception:
                self.logger.exception('failed crawling url {url}'.format(url=current_task.url))
        return results

    def crawl_urls_and_export(self, list_of_urls, depth, output_filename):
        """
        Recursively crawls a list of urls, fetching the page for each one and crawling each page's inner urls
        up to a given depth.

        Exports the results to an output file, where next to each url entry the url self referencing inner urls
        (urls withing the html page linking to the same domain) and its depth in the crawl in a TSV format.

        The pages html content will be exported to an adjacent directory named 'downloaded_urls'
        :param list_of_urls: list of formatted urls
        :param depth: depth limit
        :param output_filename: filename for crawler output summary
        :return: None
        """
        download_dir = os.path.join(os.path.dirname(output_filename), 'downloaded_urls')
        os.makedirs(download_dir, exist_ok=True)
        crawler_results = self.crawl_urls(list_of_urls, depth)

        with open(output_filename, 'w') as raw_output_file:
            output_csv_file = writer(raw_output_file, delimiter='\t')
            for crawler_result in crawler_results.values():
                # Calculate self referencing url ratio for the current url
                source_host = urlsplit(crawler_result.url).netloc
                self_referencing_urls = [url for url in crawler_result.extracted_urls if
                                         urlsplit(url).netloc == source_host]
                url_inner_reference_ratio = len(self_referencing_urls) / len(crawler_result.extracted_urls)

                # Update tab-separated output file
                output_csv_file.writerow([crawler_result.url, crawler_result.depth, url_inner_reference_ratio])

                # Write page to disk
                url_as_file_name = pathvalidate.sanitize_filepath(crawler_result.url)
                for sep in [ntpath.sep, posixpath.sep]:
                    url_as_file_name = url_as_file_name.replace(sep, '_')
                url_download_path = os.path.join(download_dir, url_as_file_name)
                with open(url_download_path, 'wb') as url_download_file:
                    url_download_file.write(crawler_result.response.content)

    def get_headers(self, url):
        """
        Safely try to fetch url headers using a HEAD request
        """
        try:
            headers_request = requests.head(url)
            headers_request.raise_for_status()
            return headers_request
        except Exception as e:
            self.logger.warning('failed querying head responses from {url}'.format(url=url))
            return None

    def get_url_info(self, url):
        """
        Query for the url content and extract urls from its html page.

        Uses cache to to reduce web access and html parsing processing based on the following parameters:
            1. Cache entry time frame since insertion. This will cause cache access whenever the same url is requested
            multiple times withing a small time frame, regardless to any other parameter
            2. Url 'Last-Modified' http header
            3. Url 'ETag' http header

        In case no relevant cache entry is found we'll request and process the page from scratch and store it
        for later.
        """

        # Check if url was cached recently enough
        if url in self.cache\
            and (datetime.now() - self.cache[url].request_time).total_seconds() < self.response_cache_time_frame_in_seconds:
            return self.cache[url]

        headers_request = self.get_headers(url)
        if headers_request:
            # Determine if can finish processing immediately based on headers info only
            headers = headers_request.headers
            if not self.should_process(headers):
                return None
            if self.is_cached(url, headers):
                return self.cache[url]

        # If the url doesn't respond to HEAD requests or if the response is not currently cached
        # We'll have to fetch the response from the web
        request_time = datetime.now()
        page_request = requests.get(url)
        page_request.raise_for_status()

        headers = page_request.headers
        if not self.should_process(headers):
            return None
        if self.is_cached(url, headers):
            return self.cache[url]

        internal_urls = self.extract_urls(page_request)
        result = UrlInfo(request_time=request_time,
                         response=page_request,
                         extracted_urls=internal_urls,)
        self.cache[url] = result
        return result

    @staticmethod
    def get_cache_validators(raw_headers):
        """
        Extract and format the headers needed for cache control.

        :param raw_headers http request headers
        :return cache validators as dict containing 'Last-Modified' and  'ETag' values
        :rtype dict
        """
        cached_validators = {'Last-Modified': None,
                             'ETag': None}

        raw_last_modified = raw_headers.get('Last-Modified')
        if raw_last_modified:
            cached_validators['Last-Modified'] = datetime.strptime(raw_headers['Last-Modified'],
                                                                   '%a, %d %b %Y %H:%M:%S GMT')
        cached_validators['ETag'] = raw_headers.get('ETag')
        return cached_validators

    def is_cached(self, url, current_headers):
        """
        Check if url is cached based on header information.

        Cases in which we'll fetch a url from the web (instead of using its cached version):
        1. url not in cache
        3. url has cache validator ('Last-Modified' or 'ETag' headers) which haven't changed
        """
        if url not in self.cache:
            return False

        cached_headers = self.cache[url].response.headers

        current_cache_validators = self.get_cache_validators(current_headers)
        previous_cache_validators = self.get_cache_validators(cached_headers)

        if current_cache_validators['Last-Modified'] and previous_cache_validators['Last-Modified'] \
            and current_cache_validators['Last-Modified'] <= previous_cache_validators['Last-Modified']:
            return True

        if current_cache_validators['ETag'] and previous_cache_validators['ETag'] \
            and current_cache_validators['ETag'] == previous_cache_validators['ETag']:
            return True
        return False

    @staticmethod
    def should_process(page_header):
        return any([allowed_mime_header in page_header.get('Content-Type', '')
                   for allowed_mime_header in Crawler.ALLOWED_MIME_TYPES])

    @staticmethod
    def extract_urls(page_request):
        html = BeautifulSoup(page_request.content, "html.parser")
        hrefs = [tag.get('href') for tag in html.find_all('a')]

        # disregard any local anchor urls (same page 'goto' links)
        hrefs = list(filter(lambda x: x and not x.startswith('#'), hrefs))
        urls = [urljoin(page_request.url, href) for href in hrefs]
        return urls
