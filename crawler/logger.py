import logging

LOGGER_NAME = 'lightricks_crawler'


def init_logger():
    logger = get_logger()
    if not logger.handlers:
        logger.addHandler(logging.StreamHandler())
        logger.setLevel(logging.INFO)


def get_logger():
    return logging.getLogger(LOGGER_NAME)


init_logger()
