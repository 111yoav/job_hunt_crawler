# job_hunt_crawler

This is a sample answer to LightTricks backend crawler task.
Implements a basic web crawler with minimal caching mechanism.

### Prerequisites
The project was currently tested under Python 3.6, for comparability with other python versions
contact me at 111yoav@gmail.com

### Installing
The project supports installation through python setup.py, i.e.

```
python setup.py install
```

or:

```
python setup.py develop
```

### Usage
The module exposes 2 main api call:

1. Crawler.crawl_urls -
        Recursively crawls a list of urls, fetching the page for each one and crawling each page's inner urls
        up to a given depth.
2. Crawler.crawl_urls_and_export -
        Recursively crawls a list of urls, fetching the page for each one and crawling each page's inner urls
        up to a given depth, then outputs the crawl summary and html pages fetched to a filesystem location.


Examples:
```
from crawler.crawler_api import Crawler

crawler = Crawler()
crawler_results = crawler.crawl_urls(['http://news.ycombinator.com/'], depth=2)
for url, crawler_result in crawler_results.items():
    #do logic
    pass
```

```
from crawler.crawler_api import Crawler

crawler = Crawler()
crawler.crawl_urls_and_export(['http://news.ycombinator.com/'], depth=2, output_filename='output_test.csv')
```

### Testing

The package is tested using py.test